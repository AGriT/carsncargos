#pragma once
#include <iostream>
#include <cstdint>

using namespace std;

class CCargo
{
public:
	CCargo(uint32_t nWeight, uint32_t nSize)
		: m_nWeight(nWeight),
		m_nSize(nSize)
	{
	}

	inline uint32_t nGetWeight() { return m_nWeight; };
	inline uint32_t nGetSize() { return m_nSize; };

	void vPrint();
	
	CCargo& operator++()
	{
		m_nWeight++;
		m_nSize++;
	}

private:
	uint32_t m_nWeight;
	uint32_t m_nSize;
};

