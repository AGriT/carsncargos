#include "CCars.h"

//TASK 2
bool CCars::blIsFit(CCargo& pclCargo)
{
	if (m_nWeight >= pclCargo.nGetWeight())
		if (!(m_nSize.has_value()) || (m_nSize.value() >= pclCargo.nGetSize()))
			return true;

	return false;
}

//TASK 5
void CCars::vPrint()
{
	cout << "Weight: " << m_nWeight << " Size: " << m_nSize.value_or(0) << endl;
}