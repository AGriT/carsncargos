#include <algorithm>
#include <iostream>
#include <vector>
#include "CNameDecorator.h"

using namespace std;

//TASK 2
void vCheckNSort(vector<CCars*>& pvclCarList, CCargo& pclCargo)
{
	for (int it = 0; it < pvclCarList.size(); it++)
		if (!pvclCarList[it]->blIsFit(pclCargo))
			pvclCarList.erase(pvclCarList.begin() + it);
}

//TASK 3
void vIncrease(int** ptrnArray, int nSize)
{
	if (nSize > 0)
	{
		*ptrnArray++;
		vIncrease(++ptrnArray, --nSize);
	}
}

//TASK 4
template <class T>
void vAddStats(vector<T>& pclToAdd)
{
	for (auto& it : pclToAdd)
		it++;
}


//TASK 6
struct stCrossRoads
{
	bool m_blIsVisited;
	vector<pair<stCrossRoads*, int>> m_vRoads;
};

void vClearVisited(stCrossRoads& stCurrent)
{
	if (!stCurrent.m_blIsVisited)
		return;

	stCurrent.m_blIsVisited = false;

	for (auto& it : stCurrent.m_vRoads)
		vClearVisited(*it.first);
}

int nCountLength(stCrossRoads& stCurrent)
{
	if (stCurrent.m_blIsVisited)
		return 0;

	int nLength = 0;
	stCurrent.m_blIsVisited = true;

	for (auto& it : stCurrent.m_vRoads)
	{
		if (!it.first->m_blIsVisited)
		{
			nLength += it.second;
			nCountLength(*it.first);
		}
	}

	return nLength;
}

int main()
{
	vector<CCars*> vclCarList;

//TASK 5
	CNameDecorator<CCars> clNamedCar(CCars(16, 16), (string)("Ford"));
	CNameDecorator<CCargo> clNamedCargo(CCargo(11, 11), (string)("Food"));

	clNamedCar.vPrint();
	clNamedCargo.vPrint();
//TASK 1
	sort(vclCarList.rbegin(), vclCarList.rend());

	return 0;
}