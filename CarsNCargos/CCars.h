#pragma once
#include <optional>
#include <iostream>
#include <tuple>
#include "CCargo.h"

using namespace std;

class CCars
{
public:
	CCars(uint32_t nWeight, uint32_t nSize)
		: m_nWeight(nWeight),
		m_nSize(nSize)
	{
	}

	CCars(uint32_t nWeight)
		: m_nWeight(nWeight)
	{
	}

	auto operator<=>(CCars& pclChecked)
	{
		if ((m_nSize.has_value()) && (pclChecked.m_nSize.has_value()))
			return (tie(m_nWeight, m_nSize.value()) <=> tie(pclChecked.m_nWeight, pclChecked.m_nSize.value()));
		else if (m_nWeight != pclChecked.m_nWeight)
			return m_nWeight <=> pclChecked.m_nWeight;
		else if (m_nSize.has_value())
			return 0 <=> m_nSize.value();
		else
			return pclChecked.m_nSize.value() <=> 0;
	}

	bool blIsFit(CCargo&);

	void vPrint();

	//TASK 4
	CCars& operator++()
	{
		m_nWeight++;

		if (m_nSize.has_value())
			m_nSize = m_nSize.value() + 1;
	}


private:
	uint32_t m_nWeight;
	optional<uint32_t> m_nSize;
};
