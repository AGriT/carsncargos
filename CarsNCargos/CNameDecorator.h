#pragma once
#include "CCars.h"

template <class T>
class CNameDecorator
{
public:
	CNameDecorator(T pObject, string strName)
		: m_pObject(pObject),
		m_strName(strName)
	{
	}

	void vPrint()
	{
		cout << "Name: " << m_strName << " ";
		m_pObject.vPrint();
	}
private:
	string m_strName;
	T m_pObject;
};

